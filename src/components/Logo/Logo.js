import React from 'react';
import PropTypes, { string } from 'prop-types';

import Logo from '../../assets/BurgerLogo.png'
import classes from './Logo.module.css';

const logo = props => {
    return (
        <div className={classes.Logo} style={{height: props.height}}>
            <img src={Logo} alt="MyBurger"/>
        </div>
    );
}

logo.propTypes = {
    height: PropTypes.string,
}

export default logo;