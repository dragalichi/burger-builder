import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import classes from './SideDrawer.module.css'
import Backdrop from '../../UI/Backdrop/Backdrop';

const sideDrawer = (props) => {
    let attacehClasses = [classes.SideDrawer, props.show ? classes.Open :classes.Close];
    return(
        <Fragment>
            <Backdrop show={props.show} close={props.close}/>
            <div className={attacehClasses.join(' ')}>
                <div onClick={props.close} className={classes.Logo}>
                    <Logo height= "80%"/>
                </div>
                <nav>
                    <NavigationItems/>
                </nav>
            </div>
        </Fragment>
    );
};

sideDrawer.propTyes = {
    show: PropTypes.bool,
    close: PropTypes.func,
};

export default sideDrawer;

