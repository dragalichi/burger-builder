import React from 'react';
import PropTypes from 'prop-types';

import classes from './DrawerToogle.module.css'

const drawerToogle = (props) => {
    return (
        <div onClick={props.clicked} className={classes.DrawerToggle}>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
};

drawerToogle.propTyes = {
    clicked: PropTypes.func,
};

export default drawerToogle;

