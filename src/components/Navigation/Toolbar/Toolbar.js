import React, { Fragment } from 'react';
import PropTypes from 'prop-types';

import classes from './Toolbar.module.css'
import Logo from '../../Logo/Logo';   
import NavigationItems from '../NavigationItems/NavigationItems';
import DrawerToogle from '../SideDrawer/DrawerToogle/DrawerToogle';

const toolbar = props => {
    return (
        <header className={classes.Toolbar}>
            <DrawerToogle clicked={props.open}/>    
            <Logo height="80%"/>
            <nav className={classes.DesktopOnly}>
                <NavigationItems/>
            </nav>
        </header>
    );
}

toolbar.propTypes = {
    open: PropTypes.func,
}

export default toolbar;