import React from 'react';
import PropTypes from 'prop-types';

import classes from './BuildControl.module.css';

const buildControls = props => {
    return (
        <div className={classes.BuildControl}>
            <button className={classes.Less} onClick={props.less} disabled={props.disabled}>Less</button>
            <div className={classes.Label}>{props.label}</div>
            <button className={classes.More} onClick={props.more}>More</button>
        </div>
    );
}

buildControls.protoType = {
    label: PropTypes.string.isRequired,
    more: PropTypes.func.isRequired,
    less: PropTypes.func.isRequired,
    disabled: PropTypes.bool,
}

export default buildControls