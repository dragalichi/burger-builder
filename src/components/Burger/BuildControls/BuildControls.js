import React from 'react';
import PropTypes from 'prop-types';

import classes from './BuildControls.module.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    {label: 'Meat', type: 'meat'},
    {label: 'Bacon', type: 'bacon'},
    {label: 'Cheese', type: 'cheese'},
    {label: 'Letucce', type: 'letucce'},
    {label: 'Tomato', type: 'tomato'},
    {label: 'Mushroom', type: 'mushroom'},
    {label: 'Onion', type: 'onion'},
    {label: 'Pickles', type: 'pickles'},
];

const buildControls = props => {
    return (
        <div className={classes.BuildControls}>
            <p>Current price: <strong>{props.price.toFixed(2)}</strong></p>
            {controls.map((ingredient)=>{
                return <BuildControl 
                key={ingredient.label+1} 
                label={ingredient.label}
                disabled={props.disableInfo[ingredient.type]}
                more={()=> props.addFunction(ingredient.type)}
                less={()=> props.removeFunction(ingredient.type)}
                />;
            })}
            <button className={classes.OrderButton} disabled={!props.canPurchase} onClick={()=> props.showSumary(true)}><strong>ORDER NOW!</strong></button>
        </div>
    );
}

buildControls.propTypes = {
    addFunction: PropTypes.func.isRequired,
    removeFunction: PropTypes.func.isRequired,
    disableInfo: PropTypes.object,
    price: PropTypes.number.isRequired,
    canPurchase: PropTypes.bool.isRequired,
    showSumary: PropTypes.func.isRequired,
};

export default buildControls;