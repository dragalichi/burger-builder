import React, {Component} from 'react'
import PropTypes from 'prop-types'

import classes from './BurgerIngredient.module.css';

class BurgerIngredient extends Component {
    render(){
        let ingredient = null;

        switch(this.props.type) {
            case ('bread-bottom'):
                ingredient = <div className={classes.BreadBottom}></div>
                break;
            case ('bread-top'):
                ingredient = (
                    <div className={classes.BreadTop}>
                        <div className={classes.Seeds1}></div>
                        <div className={classes.Seeds2}></div>
                    </div>);
                break;
            case ('meat'):
                ingredient = <div className={classes.Meat}></div>
                break;
            case ('bacon'):
                ingredient = <div className={classes.Bacon}></div>
                break;
            case ('letucce'):
                ingredient = <div className={classes.Salad}></div>
                break;
            case ('tomato'):
                ingredient = <div className={classes.Tomato}></div>
                break;
            case ('onion'):
                ingredient = <div className={classes.Onion}></div>
                break;
            case ('cheese'):
                ingredient = <div className={classes.Cheese}></div>
                break;
            case ('pickles'):
                ingredient = (
                    <div className={classes.MiniVeggies}>
                        <div className={classes.Pickles}></div>
                        <div className={classes.Pickles}></div>
                    </div>
                );
                break;
            case ('mushroom'):
                ingredient = (
                    <div className={classes.MiniVeggies}>
                        <div className={classes.Mushroom}></div>
                        <div className={classes.Mushroom}></div>
                        <div className={classes.Mushroom}></div>
                        <div className={classes.Mushroom}></div>
                        <div className={classes.Mushroom}></div>
                    </div>
                );
                break;
            default:
                break;
        }

        return ingredient;
    }
};

BurgerIngredient.propTypes = {
    type: PropTypes.string.isRequired,
};

export default BurgerIngredient;