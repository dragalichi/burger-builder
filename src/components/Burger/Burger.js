import React,{useRef, useEffect} from 'react';
import PropTypes from 'prop-types';

import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import classes from './Burger.module.css';

const Burger = props => {
    const burgerRef = useRef(null);

    const scrollToBottom = () => {
        burgerRef.current.scrollIntoView({ behavior: "smooth" })
    }

    useEffect(scrollToBottom)

    let transformedIngredients = Object.keys(props.ingredients)
        .map(igKey => {
            return [...Array(props.ingredients[igKey])].map((_, i) => {
                return <BurgerIngredient key={igKey + i} type={igKey}/>;
            });
        }).reduce((arr,el) => {
            return arr.concat(el);
        }, []).reverse();
    if(transformedIngredients.length === 0) {
        transformedIngredients = <p>Start making your burger!</p>
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top"/>
            {transformedIngredients}
            <div ref={burgerRef}></div>
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
}

Burger.propTypes = {
    ingredients: PropTypes.object.isRequired,
}

export default Burger;