import React, {Fragment}from 'react';
import PropTypes from 'prop-types';

import Button from '../../UI/Button/Button';

const orderSummary = props => {
    const ingredientSummary = Object.keys(props.ingredients).map(ingredient => {
        if(props.ingredients[ingredient] > 0) {
            return <li key={ingredient}><span style={{ textTransform: 'capitalize'}}>{ingredient}</span>: <strong>{props.ingredients[ingredient]}</strong></li>
        }
        return null;
    })

    return (
        <Fragment>
            <h3>Your Order</h3>
            <p>A delicuios burger made of:</p>
            <ul>
                {ingredientSummary}
            </ul>
            <p>The total is {props.price.toFixed(2)}</p>
            <p>Continue to checkout?</p>
            <Button btnType="Danger" onClick={props.cancel}>CANCEL</Button>
            <Button btnType="Success" onClick={props.order}>ORDER</Button>
        </Fragment>
    );
}

orderSummary.protoType = {
    ingredients: PropTypes.object.isRequired,
    cancel: PropTypes.func,
    order: PropTypes.func,
    price:PropTypes.number,
}

export default orderSummary;