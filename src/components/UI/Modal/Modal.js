import React, {Fragment, Component} from 'react';
import PropTypes from 'prop-types';

import classes from './Modal.module.css';
import Backdrop from '../Backdrop/Backdrop';

class Modal extends Component {

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }
    
    render() {
        return (
            <Fragment>
                <Backdrop show={this.props.show} close={this.props.closeFunc}/>
                {<div
                    className={classes.Modal}
                    style={{
                        transform: this.props.show ? 'translateY(0)': 'tranlateY(-100vh)',
                        opacity: this.props.show ? '1' : '0',
                        pointerEvents: this.props.show ? 'auto' : 'none',
                    }}
                    >
                    {this.props.children}
                </div>}
            </Fragment>
    
        );
    }

}

Modal.poropTypes = {
    show: PropTypes.bool,
    closeFunc: PropTypes.func.isRequired,
}


export default Modal;