﻿import React from 'react';
import PropTypes from 'prop-types';

import classes from './Spinner.module.css'

const spinner = (props) => {
    return (
        <div className={classes.Spinner}>
            Loading ...
        </div>
    );
};

spinner.propTyes = {
        
};

export default spinner;

