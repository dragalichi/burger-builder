import React from 'react';
import PropTypes from 'prop-types';

import classes from './Button.module.css';

const button = props => {
    return <button
    onClick={props.onClick}
    className={[classes.Button, classes[props.btnType]].join(' ')}
    >
        {props.children}
    </button>
}

button.propTypes = {
    onClick: PropTypes.func.isRequired,
    btnType: PropTypes.string.isRequired,
}

export default button

