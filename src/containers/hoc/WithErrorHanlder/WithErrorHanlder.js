﻿import React, {Component, Fragment} from 'react';

import Modal from '../../../components/UI/Modal/Modal';
import classes from './WithErrorHanlder.module.css'

const withErrorHanlder = (WrappedComponent, axios) => {
    return class extends Component { 
        state = {
            error: null
        }

        componentDidMount() {
            this.reqInterceptor = axios.interceptors.request.use(req => {
                this.setState({error: null});
                return req;
            })
            this.resInterceptor = axios.interceptors.response.use(res => res, error => {
                this.setState({error: error})
            });
        }

        componentWillUnmount() {
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);

        }

        render() {
            return (
                <Fragment>
                    <Modal show={this.state.error} closeFunc={()=>{this.setState({error: null})}}>
                        {this.state.error && this.state.error.message}
                    </Modal>
                    <WrappedComponent {...this.props}/>
                </Fragment>
            );
        }      
    }
};

export default withErrorHanlder;

