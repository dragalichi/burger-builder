import React, {Fragment, Component} from 'react';

import classes from './Layout.module.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component{
    state = {
        showDrawer: false,
    }

    sideDrawerShow = (value) =>{
        this.setState({showDrawer: value})
    }

    render() {
        return (
            <Fragment>
                <Toolbar open={()=>this.sideDrawerShow(true)}/>
                <SideDrawer show={this.state.showDrawer} close={()=>this.sideDrawerShow(false)} /> 
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Fragment>
        );
    }
}

export default Layout;
