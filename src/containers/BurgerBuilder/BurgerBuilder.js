import React, { Component, Fragment } from 'react';

import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHanlder from '../hoc/WithErrorHanlder/WithErrorHanlder';
import axios from '../../axios-orders';

const INGREDIENT_PRICES = {
    meat: 0.7,
    bacon: 0.5,
    cheese: 0.5,
    letucce: 0.2,
    tomato: 0.2,
    mushroom: 0.4,
    onion: 0.1,
    pickles: 0.2,
}

class BurgerBuilder extends Component {
    state = {
        ingredients:null,
        totalPrice: 4,
        canPurchase: false,
        showModal: false,
        loading: false,
    }

    componentDidMount() {
        axios.get('/Ingredients.json')
        .then(response => {
            this.setState({ingredients: response.data});
        })
    }

    getNewPrice(ingredientPrice){
        return this.state.totalPrice + ingredientPrice;
    }

    canBePurchased(updatedIngredients) {
        return Object.values(updatedIngredients).reduce((sum, el) => { return sum + el }, 0) > 0;
    }

    addIngredientHandler = (type) => {
        const newValue = this.state.ingredients[type] + 1;
        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type] = newValue;
        this.setState({ 
            ingredients: updatedIngredients, 
            totalPrice: this.getNewPrice(INGREDIENT_PRICES[type]), 
            canPurchase: this.canBePurchased(updatedIngredients),
            showModal: false,
        });

    }

    removeIngredientHandler = (type) => {
        const newValue = this.state.ingredients[type] - 1;
        if (newValue < 0) return;

        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type] = newValue;

        this.setState({ 
            ingredients: updatedIngredients,
            totalPrice: this.getNewPrice(-INGREDIENT_PRICES[type]),
            canPurchase: this.canBePurchased(updatedIngredients),
            showModal: false,
         });
    }

    showOrderHandler = (value) => {
        const newState = {
            ...this.state
        }
        newState.showModal = value;
        this.setState(newState);
    }

    purchaseHandler = () => {
        this.setState({loading: true});
        const order = {
            ingredients: this.state.ingredients,
            price: this.state.totalPrice,
            customer: {
                name: 'Pepe botellas',
                address: {
                    street: 'Av. siempre viva 123',
                    zipCode: '63003',
                    country: 'Spain',
                },
                email: 'pepe@pepe.com'
            },
            deliveryMethod: 'fastest',
        }

        axios.post('/orders.json', order)
        .then(response => {
            console.log(response)
            this.setState({loading: false});
            this.resetBurger();
        })
        .catch(error => { 
            this.setState({loading: false});
            console.log(error)
        });
    }

    resetBurger = () => {
        const price = 4;
        const ingredientMap = {...this.state.ingredients};
        Object.keys(ingredientMap).forEach((key) => {
            ingredientMap[key] = 0;
        })
        this.setState({ 
            ingredients: ingredientMap,
            totalPrice: price,
            canPurchase: false,
            showModal: false,
         });
    }

    render() {
        const disableInfo = {
            ...this.state.ingredients
        }
        for (let key in disableInfo) {
            disableInfo[key] = disableInfo[key] <= 0
        }

        let orderSummary = <Spinner/>;

        if(!this.state.loading && this.state.ingredients) {
            orderSummary = (<OrderSummary
                ingredients={this.state.ingredients}
                price={this.state.totalPrice}
                order={this.purchaseHandler}
                cancel={() => this.showOrderHandler(false)}
                />);
        }

        let burger = (
            <Fragment>
                <Burger ingredients={this.state.ingredients} />
                <BuildControls
                    addFunction={this.addIngredientHandler}
                    removeFunction={this.removeIngredientHandler}
                    disableInfo={disableInfo}
                    price={this.state.totalPrice}
                    canPurchase={this.state.canPurchase}
                    showSumary={this.showOrderHandler}
                />
            </Fragment>);
        
        if(!this.state.ingredients){
            burger = <Spinner/>
        }

        return (
            <Fragment>
                <Modal show={this.state.showModal} closeFunc={ () => this.showOrderHandler(false)}>
                    {orderSummary}
                </Modal>
                {burger}
            </Fragment>
        );
    }
}

export default withErrorHanlder(BurgerBuilder, axios);
